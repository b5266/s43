// Mock database
let posts = [];
// Movie post ID
let count = 1;

// Add post data
document.querySelector('#form-add-post').addEventListener('submit', (e) => {
	// Prevents the page 
	e.preventDefault();

	posts.push({
		id: count,
		title: document.querySelector('#txt-title').value,
		body: document.querySelector('#txt-body').value
	});

	count++

	showPosts(posts);
	alert('Movie Post Succesfully added');
})

// Show posts

const showPosts = (post) => {
	let postEntries = '';

	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`;
	})

	document.querySelector('#div-post-entries').innerHTML = postEntries;
}

// Edit post

const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
}

// update Post

document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
	e.preventDefault();

	/*
		i = 0, posts.length = 2
		posts[0].id = 1 ==='#txt-edit-id' = 2
		posts[1].id = 2 ==='#txt-edit-id' = 2
	*/

	for(let i = 0; i < posts.length; i++){

		if(posts[i].id.toString() === document.querySelector('#txt-edit-id').value){
			posts[i].title = document.querySelector('#txt-edit-title').value;
			posts[i].body = document.querySelector('#txt-edit-body').value;

			showPosts(posts);
			alert('Succesfully updated.');

			break;
		}
	}
})

// delete post

const deletePost = (id) => {
	for(let i = 0; posts.length > i; i++)
		if(posts[i].id == id){
			posts.splice(i, 1)


			showPosts(posts);
			alert ('You will no longer access this file, DELETE anyway?');

			break;
		}
};

	

/*const deletePost = (id) => {
	for()

		showPosts(posts);
		alert('Succesfully deleted.');
	})*/

